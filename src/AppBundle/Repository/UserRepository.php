<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    public function findAllPaginated($page, $rows_count)
    {
        $offset = ($page - 1) * $rows_count;

        $qb = $this->createQueryBuilder('u')
            ->setFirstResult($offset)
            ->setMaxResults($rows_count);

        return $qb->getQuery()->getResult();
    }

    public function countAll()
    {
        $qb = $this->createQueryBuilder('u')->select('COUNT(u)');

        return $qb->getQuery()->getSingleScalarResult();
    }

}