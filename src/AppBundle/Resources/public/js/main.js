$(document).ready(function() {
    $('.data-table').DataTable({
        bFilter: false,
        serverSide: true,
        ajax: $('.data-table').data('url'),
        columns: [
            {
                data: 'id',
                orderable: false,
                searchable: false,
                render: function(data, type, row, meta) {
                    var template = $('.template').clone();

                    template.find('.name').text(row.name);
                    template.find('.birthdate').text(row.birthdate);
                    template.find('.age').text(row.age);
                    template.find('.home-address').text(row.home.address);
                    template.find('.company').text(row.company.name);
                    template.find('.company-address').text(row.company.address);
                    template.find('.position').text(row.position);
                    template.find('.cv').text(row.cv);

                    return template.removeClass('hidden').html();
                }
            },
        ],
        order: [[0, 'desc']],
        pageLength: 10
    });
});