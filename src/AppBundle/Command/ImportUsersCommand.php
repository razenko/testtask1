<?php

namespace AppBundle\Command;

use DateTime;
use Exception;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Helper\ProgressBar;

class ImportUsersCommand extends ContainerAwareCommand
{

    protected $entityManager;

    protected function configure()
    {
        $this->setName('users:import')
            ->setDescription('An utility to import users from a CSV file')
            ->setHelp('Enter a file path relative to the project root folder')
            ->addArgument('file', InputArgument::REQUIRED, 'A path to the csv file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        \ini_set('memory_limit', '128M');
        //\*ini_set('max_execution_time', 30);

        $output->writeln('<bg=yellow>Disable debug with --no-debug option to avoid memory leaks!!!</>');

        $container = $this->getContainer();
        $fileSystem = $container->get('filesystem');
        $validator = $container->get('validator');
        $entityManager = $container->get('doctrine')->getManager();

        $rootDir = $container->getParameter('kernel.project_dir');
        $filePath = $rootDir . '/' . $input->getArgument('file');

        try {
            if (!$fileSystem->exists($filePath)) {
                throw new Exception("File not found on path '{$filePath}'");
            };

            $handle = fopen($filePath, "r");

            if ($handle === false) {
                throw new Exception("Can't open file on path '{$filePath}'");
            }

            $linesCount = 0;
            $maxLineLength = 0;
            $delimiter = ';';

            $output->writeln('<info>Reading the file to calculate progress...</info>');

            do {
                $data = fgetcsv($handle, $maxLineLength, $delimiter);

                $linesCount++;
            } while ($data !== false);

            if (!$linesCount) {
                throw new Exception("File doesn't contain csv data");
            }

            $output->writeln("<info>There are {$linesCount} lines with data</info>");

            $progress = new ProgressBar($output, $linesCount);

            fclose($handle);
            $handle = fopen($filePath, "r");
            $batchCount = 0;
            $batchLimit = 1000;

            $progress->start();

            do {
                $data = fgetcsv($handle, $maxLineLength, $delimiter);

                try {
                    if (!is_array($data)) {
                        throw new Exception('Not a csv line');
                    }

                    $entity = $this->getEntity($data);
                } catch (Exception $exception) {
                    $progress->advance();

                    continue;
                }

                $errors = $validator->validate($entity);

                if (count($errors)) {
                    $progress->advance();

                    // email is not unique
                    continue;
                }

                $entityManager->persist($entity);

                if (++$batchCount >= $batchLimit) {
                    $entityManager->flush();
                    $entityManager->clear();

                    unset($entity);
                }

                $progress->advance();
            } while ($data !== false);

            $progress->finish();
            $output->writeln('');
        } catch (Exception $exception) {
            $error = $exception->getMessage();
            $output->writeln("<error>{$error}</error>");

            return;
        }

        $output->writeln('<info>The file completely imported</info>');
    }

    protected function getEntity(array $data)
    {
        $fields = [
            'FirstName', 'LastName', 'Birthdate', 'Email', 'HomeCity',
            'HomeZip', 'HomeAddress', 'Phone', 'CompanyName', 'WorkCity',
            'WorkAddress', 'Position', 'Cv',
        ];

        $entity = new User();

        foreach ($fields as $index => $field) {
            $value = isset($data[$index]) ? $data[$index] : null;

            if (!$value) {
                throw new Exception('Empty field');
            }

            $value = $field == 'Birthdate' ? $this->getDate($value) : $value;

            $setter = "set{$field}";
            $entity->{$setter}($value);
        }

        return $entity;
    }

    protected function getDate($value)
    {
        $dateTime = DateTime::createFromFormat('d.m.Y', $value);

        if (!is_object($dateTime)) {
            throw new Exception('Invalid date');
        }

        return $dateTime;
    }

}
