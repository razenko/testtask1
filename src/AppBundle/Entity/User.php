<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity("email")
 */
class User
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_name;

    /**
     * @ORM\Column(type="date")
     */
    private $birthdate;

    /**
     * @ORM\Column(type="string", unique=true, length=255)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $home_city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $home_zip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $home_address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $company_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $work_city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $work_address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $position;

    /**
     * @ORM\Column(type="text")
     */
    private $cv;

    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Get full name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * 
     * @return string
     */
    public function getAge()
    {
        $dateRange = $this->birthdate->diff(new DateTime('now'));

        return $dateRange->y;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set homeCity
     *
     * @param string $homeCity
     *
     * @return User
     */
    public function setHomeCity($homeCity)
    {
        $this->home_city = $homeCity;

        return $this;
    }

    /**
     * Get homeCity
     *
     * @return string
     */
    public function getHomeCity()
    {
        return $this->home_city;
    }

    /**
     * Set homeZip
     *
     * @param string $homeZip
     *
     * @return User
     */
    public function setHomeZip($homeZip)
    {
        $this->home_zip = $homeZip;

        return $this;
    }

    /**
     * Get homeZip
     *
     * @return string
     */
    public function getHomeZip()
    {
        return $this->home_zip;
    }

    /**
     * Set homeAddress
     *
     * @param string $homeAddress
     *
     * @return User
     */
    public function setHomeAddress($homeAddress)
    {
        $this->home_address = $homeAddress;

        return $this;
    }

    /**
     * Get homeAddress
     *
     * @return string
     */
    public function getHomeAddress()
    {
        return $this->home_address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return User
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set workCity
     *
     * @param string $workCity
     *
     * @return User
     */
    public function setWorkCity($workCity)
    {
        $this->work_city = $workCity;

        return $this;
    }

    /**
     * Get workCity
     *
     * @return string
     */
    public function getWorkCity()
    {
        return $this->work_city;
    }

    /**
     * Set workAddress
     *
     * @param string $workAddress
     *
     * @return User
     */
    public function setWorkAddress($workAddress)
    {
        $this->work_address = $workAddress;

        return $this;
    }

    /**
     * Get workAddress
     *
     * @return string
     */
    public function getWorkAddress()
    {
        return $this->work_address;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return User
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set cv
     *
     * @param string $cv
     *
     * @return User
     */
    public function setCv($cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * Get cv
     *
     * @return string
     */
    public function getCv()
    {
        return $this->cv;
    }
}
